<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
  xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>1D Oscillator</title>
    <authors>
      <author>Alexander Schmiedhofer</author>
      <author>Florian Toth</author>
    </authors>
    <date>2021-10-10</date>
    <keywords>
      <keyword>mechanic</keyword>
    </keywords>
    <references></references>
    <isVerified>yes</isVerified>
    <description>
    A single degree of freedom oscillator composed of a square mass (1mx1m) with density 1 kg/m3 in 2d (plane strain).
    We constrain in x direction and connect a concentrated stiffness and damping in the y direction.

    The single-dof ocillator is
    m*x''+c*x'+l*x = f
    or in the narmalised from
    x'' + 2*xi*w_0 x' + w_0^2 x = f/m
    with
    w_0 = sqrt(k/m)
    xi = 2*m*w_0
    The iegenvalues of the characteristic equation are
    lambda = -xi*w_0 +- w_0*sqrt( xi^2 - 1 )
    for xi lower than 1 we get oscillatory solutions (complex valued eigenvalues).

    The resulting system has a mass m=1, we set k=1, thus, the (un-daped) natural frequency is w_0=2*pi Hz.
    Setting xi=3/5, i.e. the damping constant c=2*m*w_0*xi=2*1*1*3/5, we obtain 
    Re(lambda) = 3/5 = 0.6 and Im(lambda) = 4/5 = 0.8
    </description>
  </documentation>
  <fileFormats>
    <input>
      <!-- <gmsh fileName="UnitSquare_quad.msh"/> -->
      <hdf5 fileName="1dOscillator_eigenFrequency_firstCompanion.h5ref"/>
    </input>
    <output>
      <hdf5/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  <domain geometryType='plane'  printGridInfo="no">
    <variableList>
      <var name="T" value="0.1"/>
    </variableList>
    <regionList>
      <region name="surf" material="myOne" />
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
         <eigenFrequency>
          <isQuadratic>yes</isQuadratic>
          <!-- <freqShift>0</freqShift>
          <numModes>4</numModes> -->
          <minVal>-10</minVal>
          <maxVal>10</maxVal>
          <writeModes>yes</writeModes>
        </eigenFrequency> 
    </analysis>  
    <pdeList>
      <mechanic subType='planeStrain'>
        <regionList>
          <region name="surf" complexMaterial="no"/>
        </regionList>        
        <bcsAndLoads>
          <fix name="W">
            <comp dof="x"/>
          </fix>
          <concentratedElem name="SW" dof="y" stiffnessValue="1.0" dampingValue="3/5*2"/>
        </bcsAndLoads>      
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
            <nodeList>
              <nodes name="SW" outputIds="txt"/>
            </nodeList>
          </nodeResult>
        </storeResults>
      </mechanic>
    </pdeList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <setup idbcHandling="elimination" />
            <!-- <exportLinSys mass="true" stiffness="true" damping="true"/> -->
            <matrix reordering="default"/>
            <eigenSolver id="myquadratic"/>
          </standard>
        </solutionStrategy>
        <eigenSolverList>
          <quadratic id="myquadratic">
            <generalisedEigenSolver id="mygen"/>
            <linearisation>firstCompanion</linearisation>
          </quadratic>
          <feast id="mygen">
            <logging>true</logging>
            <stopCrit>6</stopCrit>
          </feast>
          <arpack id="ap"></arpack>
        </eigenSolverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>