Test for Mechanic Acoustic Eigenvalue Analyis
=============================================

* acoustic pressure formulation
* uses non-conforming grids (NMG) based on Mortar method
* non-symmetric gneralised EVP solved with FEAST
* results match [conforming case](../CoupledEV2Dpressure) 
  and [conforming potential formulation case](../CoupledEV2D)
