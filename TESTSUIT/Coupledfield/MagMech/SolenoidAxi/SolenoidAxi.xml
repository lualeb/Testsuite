<?xml version="1.0" encoding='UTF-8'?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>Stress Analysis of Long, Thick Isotropic Solenoid</title>
    <authors>
    <author>ahauck</author>
    </authors>
    <date>2010-06-22</date>
    <keywords>
      <keyword>coil</keyword>
   <!--   <keyword>magneto-mechanic</keyword>
      <keyword>ansys verifcation example</keyword>-->
    </keywords>
    <references>
       Ansys Verification Example VM 172, 
       based on  F.C. Moon, "Magneto-Solid Mechanics",
       1984, pp. 110ff
    </references>
    <isVerified>yes</isVerified>
    <description>
      A long, thick solenoid carries a uniform current density distribution, J. 
      Assuming that the turns of the solenoid can be modeled as a homogeneous 
      isotropic material with modulus of elasticity E, and Poisson's ratio ν, 
      determine the axial magnetic flux density distribution Bθ and the 
      circumferential stress σo distribution in the solenoid.
        
      We compare the axial flux density and the cirumferential stress in 
      radial direction. 
      
      The detailed description can be found in ansys-vm-172-description.pdf.
    </description>
    </documentation>
  <fileFormats>
    <output>
      <hdf5/>
      <!-- <text id="txt" fileCollect="timeFreq"/>--> 
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="axi">
    <regionList>
      <region name="air"      material="iso"/>
      <region name="solenoid" material="iso"/>
    </regionList>
    <elemList>
      <!-- Select list of elements, at which we compare the 
           mag. flux density and the mech. stress with the
           ANSYS reference solution -->
      <elems name="histElems">
        <list>
          <freeCoord comp="x" start="0.01" stop="0.02" inc="2e-4"/>
          <fixedCoord comp="y" value="0"/>
        </list>
      </elems>
    </elemList>
  </domain>

  <sequenceStep>
    <analysis>
       <static/> 
    </analysis>
    <pdeList>

      <magnetic>
        <regionList>
          <region name="air"/>
          <region name="solenoid"/>
        </regionList>

        <bcsAndLoads>
          <fluxParallel   name="left" >
	    <comp dof="r"/>
	  </fluxParallel>
          <!--<constraint name="right" />-->
         <!-- <fluxParallel   name="right">
	    <comp dof="r"/>
	  </fluxParallel>-->
        </bcsAndLoads>
        
	<coilList>
	<!-- The excitation in the ANSYS example is 
             J = 1e6 A/m^2. The cross section of the
             coil is 2e-5, so when dviding the current 
             of 20 A over the area, we obtain J = 1e6 A/m^2 -->
           <coil id="myCoil">
            <source type="current" value="20"/>
              <part>
                <regionList>
                  <region name="solenoid"/>
                </regionList>
		<direction>
		  <analytic/>
		</direction>
                <wireCrossSection area="2e-5"/>
                <resistance value="0"/> 
              </part>
            </coil>
          </coilList>

        <storeResults>
          <nodeResult type="magPotential">
            <allRegions/>
          </nodeResult>
          <elemResult type="magFluxDensity">
            <allRegions/>
            <!-- Write B-field for history elements separately -->
            <elemList>
              <elems name="histElems"/>
            </elemList>
          </elemResult>
          <elemResult type="magForceLorentzDensity">
            <allRegions/>
          </elemResult>
          <regionResult type="magForceLorentz">
            <allRegions/>
          </regionResult>
<!--            -->
        </storeResults>
      </magnetic>
      
      
      <mechanic subType="axi">
        <regionList>
          <region name="solenoid" softeningId="1"/>
        </regionList>
   
        <!-- Note: The ANSYS element type PLANE13 as used in the 
             reference example always uses additional shape functions
             by default, which corresponds to the use of Taylor Wilson
             softening in CFS -->
        <softeningList>
          <icModesTW id="1"/>
        </softeningList>
        
        <bcsAndLoads>
          <!-- The ring is allowd to slide in r-direction at 
               the bottom -->
	   <fix name="lower">
            <comp dof="z"/>
	  </fix>
	  
	  <!-- ========================= -->
	  <!--  Iterative Coupling       -->
	  <!-- ========================= -->
	  <forceDensity name="solenoid">
	    <coupling pdeName="magnetic">
	      <quantity name="magForceLorentzDensity"/>
	    </coupling>
	  </forceDensity>
	  
          <!-- The upper nodes are coupled using constraints in
               z-direction to get uniform deformation -->
          <constraint name="upper" masterDof="z" slaveDof="z"/>
        </bcsAndLoads>
        <storeResults>
          <elemResult type="mechStress">
            <allRegions/>
            
            <elemList>
              <elems name="histElems"/>
            </elemList>
          </elemResult>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
    </pdeList>
    
    <couplingList>
      <iterative>
        <convergence logging="yes" maxNumIters="1" stopOnDivergence="no"/>
      </iterative>
    </couplingList>

    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>


  </sequenceStep>
</cfsSimulation>
