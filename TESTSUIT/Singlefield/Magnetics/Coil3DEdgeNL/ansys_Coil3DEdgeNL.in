! ========================================================================
!  3D Model of a straight conductor in air.
! ========================================================================
fini
/uis,msgpop,3        ! disable warning popups
/clear
/filname,Coil3DEdgeNL,1       
/prep7

! initialize macros for .mesh-interface
init


! define geometries
coil_w = 1
core_w = 4

conduc_h = 3
air_h = 3

esz = 1


rectng,0,coil_w,0,coil_w
rectng,coil_w,coil_w+core_w,0,coil_w


rectng,0,coil_w,coil_w,coil_w+core_w
rectng,coil_w,coil_w+core_w,coil_w,coil_w+core_w


allsel
nummrg,kp

! generate area mesh
setelems,'quadr'
!setelems,'triangle'
esize,esz
mshkey,0
amesh,all

! extrude conductor height
setelems,'brick'
allsel
esize,,conduc_h/esz
!VEXT, NA1, NA2, NINC, DX, DY, DZ, RX, RY, RZ
vext,all,,,0,0,conduc_h

! extrude air height
allsel
esize,,air_h/esz
asel,s,loc,z,conduc_h
vext,all,,,0,0,air_h

! generate surface mesh on exterior areas
allsel
asel,s,ext
setelems,'quadr'
amesh,all


! compres element numbers
allsel
numcmp,elem
allsel
nummrg,node
numcmp,node

! define components

! a) coil
allsel
vsel,s,loc,x,0,coil_w
vsel,r,loc,y,0,coil_w
vsel,r,loc,z,0,conduc_h
cm,coil,volume

! b) coil-air
allsel
vsel,s,loc,x,0,coil_w
vsel,r,loc,y,0,coil_w
vsel,r,loc,z,conduc_h,conduc_h+air_h
cm,coil-air,volume

! c) air
allsel
vsel,s,loc,x,0,coil_w+core_w
vsel,r,loc,y,0,coil_w+core_w
vsel,r,loc,z,0,conduc_h,
cmsel,u,coil
cm,air,volume

! d) air-air
allsel
vsel,s,loc,x,0,coil_w+core_w
vsel,r,loc,y,0,coil_w+core_w
vsel,r,loc,z,conduc_h,conduc_h+air_h
cmsel,u,coil-air
cm,air-air,volume

! ==== Write mesh ===
allsel
nsel,all
wnodes

! -- Regions
cmsel,s,coil
cmsel,a,coil-air
eslv
welems,'coil'
'

cmsel,s,air
cmsel,a,air-air
eslv
welems,'air'


! -- Surfaces / Nodes
outer_pos = coil_w+core_w
top_pos =  conduc_h+air_h

asel,s,loc,z,0
esla
welems,'botSurf'
nsla,s,1
wnodbc,'botNodes'

asel,s,loc,z,top_pos
esla
welems,'topSurf'
nsla,s,1
wnodbc,'topNodes'

asel,s,loc,x,outer_pos
esla
welems,'xOuterSurf'
nsla,s,1
wnodbc,'xOuterNodes'

asel,s,loc,x,0
esla
welems,'xInnerSurf'
nsla,s,1
wnodbc,'xInnerNodes'

asel,s,loc,y,outer_pos
esla
welems,'yOuterSurf'
nsla,s,1
wnodbc,'yOuterNodes'

asel,s,loc,y,0
esla
welems,'yInnerSurf'
nsla,s,1
wnodbc,'yInnerNodes'


mkmesh
