<?xml version='1.0' encoding='UTF-8'?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <documentation>
    <title>Simply supported cantilever made up of two Nitsche-coupled regions
    with differing ansatz order</title>
    <authors> 
      <author>Jens Grabinger</author>
    </authors>
    <date>2018-03-13</date>
    <keywords>
      <keyword>eigenFrequency</keyword>
      <keyword>mechanic</keyword>
      <keyword>nonmatching grids</keyword>
    </keywords>
    <references>NACS</references>
    <isVerified>yes</isVerified>
    <description>
      A modal analysis of a simply supported cantilever, which is made up of two
      regions with different discretization (1st order hex vs. 2nd order wedge
      mesh), is performed. Both regions are coupled using the Nitsche approach.
      The computed mode shapes must be symmetric, otherwise the Nitsche coupling
      is flawed (usually due to different integration order).
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5/>
    </input>
    <output>
      <hdf5 id="hdf"/>
    </output>
    <materialData/>
  </fileFormats>

  <domain geometryType="3d">
    <regionList>
      <region material="Aluminum" name="cantilever1"/>
      <region material="Aluminum" name="cantilever2"/>
    </regionList>
    <ncInterfaceList>
      <ncInterface masterSide="cantilever2-nc1-bot" name="nc1_1_" slaveSide="cantilever1-nc1-top"/>
    </ncInterfaceList>
  </domain>

  <fePolynomialList>
    <Lagrange id="_id_lagrange_iso_1">
      <isoOrder>1</isoOrder>
    </Lagrange>
    <Lagrange id="_id_lagrange_iso_2">
      <isoOrder>2</isoOrder>
    </Lagrange>
  </fePolynomialList>

  <sequenceStep index="1">
    <analysis>
      <eigenFrequency>
        <isQuadratic>no</isQuadratic>
        <numModes>20</numModes>
        <freqShift>5.0</freqShift>
        <writeModes>yes</writeModes>
      </eigenFrequency>
    </analysis>
    
    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="cantilever1" polyId="_id_lagrange_iso_1"/>
          <region name="cantilever2" polyId="_id_lagrange_iso_2"/>
        </regionList>

        <ncInterfaceList>
          <ncInterface formulation="Nitsche" name="nc1_1_" nitscheFactor="10" nitscheFactorDamping="0.01"/>
        </ncInterfaceList>

        <bcsAndLoads>
          <fix name="fix1">
            <comp dof="x"/>
            <comp dof="y"/>
            <comp dof="z"/>
          </fix>
          <fix name="fix2">
            <comp dof="x"/>
            <comp dof="y"/>
            <comp dof="z"/>
          </fix>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="mechDisplacement" complexFormat="amplPhase">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>
</cfsSimulation>
